#!/usr/bin/env python
from typing import Set, NoReturn
import json
import os
import shutil

VIDEO_EXTENSIONS = [".mkv", ".mp4", ".webm", ".avi"]
VIDEOS_DIR = os.path.expanduser("~/Videos")
DB_PATH = os.path.expanduser("~/.config/FreeTube/savedvideos.db")
YDL_TEMPLATE = "youtube-dl https://www.youtube.com/watch?v={{ID}}"


def is_video_file(path: str) -> bool:
    _, ext = os.path.splitext(path)
    return ext in VIDEO_EXTENSIONS


def list_videos() -> Set[str]:
    return {
        path for entry in os.scandir(VIDEOS_DIR) if is_video_file(path := entry.path)
    }


def favorite_ids() -> Set[str]:
    with open(DB_PATH, "r") as f:
        return {video_id for line in f if (video_id := json.loads(line).get("videoId"))}


def trash(videos: Set[str], favorite_ids: Set[str]) -> Set[str]:
    keep = {path for path in videos for id_ in favorite_ids if id_ in path}
    return videos.difference(keep)


def remove_trash(trash_videos: Set[str]) -> NoReturn:
    trash_dir = os.path.join(VIDEOS_DIR, "Trash")
    os.makedirs(trash_dir, exist_ok=True)
    for path in trash_videos:
        shutil.move(path, trash_dir)


def make_ydl_file(ids: Set[str]) -> str:
    return "\n".join(
        ["#!/bin/bash", f"cd {VIDEOS_DIR}"]
        + [YDL_TEMPLATE.replace("{{ID}}", id_) for id_ in ids]
    )


def create_ydl_file(ids: Set[str]) -> NoReturn:
    path = ".ydl.sh"
    with open(path, "w") as f:
        f.write(make_ydl_file(ids))
    os.chmod(path, 0o777)


def main():
    videos = list_videos()
    favorites = favorite_ids()
    remove_trash(trash(videos, favorites))
    create_ydl_file(favorites)


if __name__ == "__main__":
    main()
